=============
1. PrePanel
=============

PrePanel 프로그램은 IUPAN 프로그램의 전처리 프로그램이다.

-  언어 : Fortran 90
-  input : grid.cgns, wake.inp
-  output : grid.con, grid.grd, inp.dat
-  lib : CGNS 4.x.x, HDF5

------------------
1.1 PrePanel 파일
------------------




1.1.1 grid.cgns
================

CGNS란 CFD General Notation System의 약어로 2D/3D로 작성된 격자의
확장자이다.

CGNS파일은 HDF5 형식으로 작성되어 일반적인 text editor로는 읽을 수 없고,
cgns library에 포함되어 있는 cgnsview, cgnsplot, cgnslist 등으로 정보를
확인할 수 있다.

Pointwise로 표면 격자 구성시 유의 사항은 다음과 같다.

1. CAE option은 Unstructured, 2-D를 사용한다. IUPAN은 비정렬 격자 기반의
   솔버이며, Panel method는 표면 격자를 2차원으로 사용하기 때문이다.

   예시는 다음과 같다.

   .. raw:: html

      <center>

   .. figure:: image/CAE_option.png
      :alt: CAE option

   .. raw:: html

      </center>

2. 모든 Patch의 normal vector는 물체 표면의 outward를 향한다. 예시는
   다음과 같다.

   .. raw:: html

      <center>

   .. figure:: image/normal_vector.png
      :alt: Normal vector direction

   .. raw:: html

      </center>

3. 격자를 출력할 시 다음의 옵션을 사용한다.

   .. raw:: html

      <center>

   .. figure:: image/export_option.png
      :alt: Export option

   .. raw:: html

      </center>

1.1.2 wake.inp
================

wake.inp 파일은 wake panel이 생성되는 connector의 정보를 작성하는
파일이다. 예시는 다음과 같다.

.. code-block:: text


                nwake1
                     1
           wakeconname
               con-1
                nwake2
                     0
           wakeconname

변수의 의미는 다음과 같다.

-  nwake1 : wake type 1인 connector의 개수
-  nwake2 : wake type 2인 connector의 개수
-  wakeconname : 해당 type의 connector 이름(nwake1, nwake2 만큼 존재)

※ CGNS library는 connector의 이름을 17글자까지만 인식할 수 있으므로 17글자를 넘지 않도록 한다.

--------------

Wake type은 다음과 같이 분류된다.

-  Type 1 : 하류 방향으로 wake 패널을 생성하는 connector

   -  날개의 Trailing edge connector
   -  wake 면의 하류 방향에서 형상의 폭이 감소하여 하류 방향 wake 면을
      생성하는 connector

-  Type 2 : 하류 방향으로 wake 패널을 생성하지 않고 wake 면이 맞닿는
   connector

그림으로 나타내면 다음과 같다.

.. raw:: html

   <center>

.. figure:: image/wake_type.png
   :alt: alt text

   alt text

.. raw:: html

   </center>

1.1.3 grid.con
================

grid.con 파일은 각 domain의 연결 정보를 나타내는 파일이다.

파일 구조는 다음과 같다.

.. code-block:: text

                  nconns
                       2
         current connect                npnts                wflag
                       1                   2                    0
            current zone                 pnts           donor zone                 pnts
                       1                  401                    2                  410
                       1                  402                    2                  409
         current connect                npnts                wflag
                       2                   3                    1
            current zone                 pnts           donor zone                 pnts
                       1                    1                    2                   10
                       1                    2                    2                    9
                       1                    3                    2                    8
                       1                    4                    2                    7

변수의 의미는 다음과 같다.

- nconns : 연결 정보가 들어있는 connector의 총 개수

---------------------

- nconns 만큼 반복 
- currunt connect : 현재 connector의 index 
- npnts : 현재 connector의 node 수 
- wflg : wake connector option (0 : 일반, 1 : type 1, 2 : type 2)

--------------

- npnts 만큼 반복
- currunt zone : 현재 domain의 index
- pnts : 현재 domain에서의 node의 index
- donor zone : 연결된 zone의 index
- pnts : 연결된 domain에서의 node의 index

1.1.4 grid.grd
================

grid.grd 파일은 CGNS gridfile을 Tecplot unstructured grid 형식으로
변환한 file이다.

파일 구조는 다음과 같다.

.. code-block:: text

     TITLE = "unstructured grid"
     VARIABLE = "x", "y", "z"
     ZONE NODES =                   5 , ELEMENTS =                   7 , DATAPACKING=POINT,ZONETYPE=FEQUADRILATERAL
     X                                           Y                                             Z
     X                                           Y                                             Z
     X                                           Y                                             Z
     X                                           Y                                             Z
     X                                           Y                                             Z
     nx                                          ny                                            nz
     nx                                          ny                                            nz
     nx                                          ny                                            nz
     nx                                          ny                                            nz
     nx                                          ny                                            nz
     nx                                          ny                                            nz
     nx                                          ny                                            nz

변수의 의미는 다음과 같다.

--------------

- domain 개수만큼 반복
- ZONE NODES : node 개수
- ELEMENTS : cell 개수
- X, Y, Z : 현재 node의 좌표
- nx, ny, nz : cell을 이루는 3~4개의 node의 index

1.1.5 inp.dat
================

inp.dat 파일은 IUPAN의 input file에 들어가는 격자 정보이다.

파일 구조는 다음과 같다.

.. code-block:: text

              grid input
              6
        157     974     947      45      22      48

        244    1837    1763      61      20      67

              wake input
                       0

파일의 정보는 한 줄씩

- grid input
- 총 domain의 수
- 각 domain 의 node 수
- 각 domain 의 panel 수

--------------------------

- wake input
- 총 wake connector 의 수
- 각 wake connector 의 번호
- 각 wake connector 의 node 수

-----------------------
1.2 PrePanel 사용 방법
-----------------------

PrePanel을 사용하기 위한 과정은 다음과 같다.

1. 격자 생성 프로그램(PointWise)을 통해 물체 표면 격자(CGNS format)를
   생성한다.
2. 물체 표면에서 wake panel이 생성되는 connector의 정보를 wake.inp에
   입력한다.
3. 물체 표면 격자의 이름을 **grid.cgns** 로 변경하고, wake.inp 파일과
   같은 경로에 위치한다.
4. 해당 경로에서 PrePanel 프로그램을 실행하면 grid.con, grid.grd 파일이
   생성된다.
