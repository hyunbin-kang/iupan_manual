==============
2. IUPAN
==============

IUPAN 프로그램은 3차원 비정렬 Panel method 기반의 유동 해석자이다.

-  언어 : Fortran 90
-  input : grid.con, grid.grd, panel.inp
-  output : fnm.out, fnms.out, coef.dat, conv.dat, grid.nXX.tXX.plt,
   grid.nXX.tXXc.plt, gridg.plt

--------------
2.1 IUPAN 파일
--------------

2.1.1 panel.inp
================

IUPAN의 input file로 수치, 유동, 격자에 대한 정보가 포함되어 있다.

Fortran의 namelist 기능을 이용해 변수들을 구성하고 있다.

파일 구성은 다음과 같다.

.. code-block:: text


   grid
         icmx      icmy      icmz      cmax
         5000      5000      5000       500
    &calcon
    std     = 1,
    dt      = 0.005,
    tend    = 0.0,
    nwke    = 1,
    nprt    = 1, 
    iter    = 1,
    theta   = 0.0,
    dtau    = 1.0e10,
    ot      = 1.0
    /

    &flocon
    aoa     = 5.0,
    aos     = 0.0,
    mach    = 0.8,
    p0      = 0.0,
    q0      = 0.0,
    r0      = 0.0,
    icp     = 1,
    body    = 1,
    ipqr     = 0,
    amp     = 0.0,
    h       = 0.0,
    rdf     = 0.0
    /

    &inppan
    grdfile = 'grid',
    ptchi   = 25,
    nodei   = 40, 70, 70, 29, 2632, 40, 70, 70, 44, 25,
              98, 98, 70, 70, 25, 44, 7, 7, 70, 70,
              17, 29, 29, 17, 29, 
    pani    = 56, 52, 52, 30, 2568, 56, 52, 52, 30, 18,
              78, 78, 52, 52, 18, 30, 3, 3, 52, 52,
              12, 30, 30, 12, 30,
    ptsmi   = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
              0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
              0, 0, 0, 0, 0,
    wcni    = 12,
    wki     = 8, 12, 21, 28, 29, 33, 38, 44, 49, 64,
              66, 69,
    wpnti   = 5, 5, 8, 8, 4, 4, 7, 7, 5, 5, 
              5, 5,
    /

    &inpbdy
    xcgi    = 2264.0,
    ycgi    = 0.0,
    zcgi    = 12.0,
    srfi    = 1600000.0,
    xrfi    = 3170.75,
    yrfi    = 505.143,
    zrfi    = 3170.75,
    /

변수에 대한 내용은 다음과 같다.

.. raw:: html

   <details>

변수 설명

&calcon 해석 기법에 대한 namelist 

- std : steady/unsteady option

  - std=0 : unsteady

  -  std=1 : steady

- dt : unsteady 계산에서의 
- time step 
- tend : unsteady 계산에서의 tend 
- nwke : unsteady 계산에서의 하류방향 최소 wake panel 수 
- nprt : Tecplot 파일 출력 step 간격 
- iter : unsteady 계산에서의 최대 iteration 수 
- theta : 이중 시간 적분법에 사용되는 이산화 방법

  -  theta=1 : Implicit method

  -  theta=0.5 : Trapezoidal method

  -  theta=0.0 : Explicit method

- dtau : 이중 시간 적분법에 사용되는 parameter ot : 시간 이산화 오차 order

  -  ot=1 : 1st order

  -  ot=2 : 2nd order

--------------

&flocon 유동 조건에 대한 namelist 

- aoa : angle of alpha(deg) 
- aos : angle of sideslip(deg) 
- mach : mach number 
- p0 : angular velocity 
- q0 : angular velocity
- r0 : angular velocity
- icp : Compressible option

  -  icp=0 : incompressible
  -  icp=1 : compressible

- body : 해석 물체 수
- ipqr : Oscillation 방향 
- amp : Oscillation amplitude
- h : plunging amplitude 
- rdf : reduced frequency()

--------------

&inppan 패널 정보에 대한 namelist, body만큼 존재, inp.dat 파일에서 생성

- grdfile : 격자 파일 이름(확장자 제외)
- ptchi : domain(patch) 개수
- nodei : 각 domain(patch)의 node수 
- pani : 각 domain(patch)의 panel수

.. raw:: html

   </details>




2.1.2 fnm.out
================

fnm.out 파일은 body axis를 기준으로 Force & Moment coefficient를
출력하는 파일이다. 파일 구조는 다음과 같다.

.. code-block:: text

    Force and Moment Coefficients (body axis)
    
    Patch #             Cx             Cy             Cz             Cl             Cm             Cn
          1  0.0000000E+00 -0.4794637E-02  0.0000000E+00  0.2627051E-05  0.0000000E+00  0.2813039E-03
          2 -0.1107633E-02  0.3975705E-02 -0.5790722E-01 -0.7144616E-02  0.4285360E-02  0.8774381E-03
          ...
          5  0.0000000E+00  0.0000000E+00  0.2441489E-02  0.4475482E-03 -0.3578409E-03  0.0000000E+00
          6 -0.1500686E-04  0.6520431E-02  0.3379182E-05 -0.3400089E-03 -0.1223390E-05  0.9500030E-03
    
     Body #             Cx             Cy             Cz             Cl             Cm             Cn
          1 -0.2247292E-02  0.2739590E-02  0.2389490E-02  0.1102963E-03  0.4717192E-03  0.2368874E-02
    
    Patch #             Cx             Cy             Cz             Cl             Cm             Cn
          7  0.8840194E-11  0.5102573E-04  0.5102582E-04  0.1292638E-05 -0.8399035E-05  0.8399020E-05
          8  0.1917130E-10 -0.5930057E-04  0.5930052E-04 -0.2292973E-05 -0.9897720E-05 -0.9897725E-05
          ...
         22  0.1822968E-04  0.6219889E-03 -0.5509988E-03 -0.1214362E-04  0.9257277E-04  0.1020931E-03
         23  0.4558401E-05 -0.6533189E-03  0.5540899E-03 -0.6144642E-04 -0.9093980E-04 -0.1038518E-03
    
     Body #             Cx             Cy             Cz             Cl             Cm             Cn
          2 -0.5198542E-03 -0.7171273E-03 -0.2623438E-02 -0.5710946E-05  0.3962378E-03  0.5047637E-04

domain에 대한 공력 계수는 body 별 domain 개수 만큼 반복 되며, body에
대한 공력 계수는 body 개수 만큼 반복 된다.

만약 Unsteady 계산의 경우 다음과 같이 반복된다.

.. code-block:: text

    Force and Moment Coefficients
    
    Patch #             Cx             Cy             Cz             Cl             Cm             Cn          #t1
          1  0.0000000E+00  0.1526637E-05  0.0000000E+00  0.4627074E-11  0.0000000E+00  0.2116846E-08
          2  0.1320348E-04  0.0000000E+00 -0.1565759E+00  0.6272906E-09  0.4491736E-02 -0.3010687E-10
          3  0.1320348E-04  0.0000000E+00  0.1565759E+00  0.6272906E-09 -0.4491736E-02  0.3010690E-10
          4  0.0000000E+00 -0.1526637E-05  0.0000000E+00  0.4627074E-11  0.0000000E+00 -0.2116846E-08
    
     Body #             Cx             Cy             Cz             Cl             Cm             Cn
          1  0.2640697E-04 -0.3985078E-17  0.1099121E-12  0.1263835E-08 -0.2627759E-13  0.3626481E-16
    
    Patch #             Cx             Cy             Cz             Cl             Cm             Cn          #t2
          1  0.0000000E+00  0.1699430E-05  0.0000000E+00  0.7896368E-12  0.0000000E+00  0.2560423E-08
          2 -0.6642847E-01  0.0000000E+00  0.2167507E+01  0.9425765E-09 -0.6103694E+00 -0.2263167E-10
          3  0.6626627E-01  0.0000000E+00  0.2481528E+01  0.3105491E-09 -0.6196500E+00  0.3768889E-10
          4  0.0000000E+00 -0.1698346E-05  0.0000000E+00  0.8503016E-11  0.0000000E+00 -0.2514425E-08
    
     Body #             Cx             Cy             Cz             Cl             Cm             Cn
          1 -0.1621995E-03  0.1084196E-08  0.4649034E+01  0.1262418E-08 -0.1230019E+01  0.6105504E-10
    
    Patch #             Cx             Cy             Cz             Cl             Cm             Cn          #t3
          1  0.0000000E+00  0.1692940E-05  0.0000000E+00  0.3612960E-10  0.0000000E+00  0.2511798E-08
          2  0.8357455E-03  0.0000000E+00 -0.9841933E-01  0.5679332E-09 -0.1171626E-01 -0.2449756E-10
          3 -0.1031651E-02  0.0000000E+00  0.2153614E+00  0.6852809E-09 -0.2081348E-01  0.3581279E-10
          4  0.0000000E+00 -0.1708135E-05  0.0000000E+00 -0.2683631E-10  0.0000000E+00 -0.2572260E-08
    
     Body #             Cx             Cy             Cz             Cl             Cm             Cn
          1 -0.1959058E-03 -0.1519529E-07  0.1169421E+00  0.1262507E-08 -0.3252974E-01 -0.4914651E-10
    
    Patch #             Cx             Cy             Cz             Cl             Cm             Cn          #t4
          1  0.0000000E+00  0.1695562E-05  0.0000000E+00  0.3624543E-10  0.0000000E+00  0.2525471E-08
          2  0.3090506E-02  0.0000000E+00 -0.1268733E+00  0.6293509E-09  0.2544998E-02 -0.2412419E-10
          3 -0.3288937E-02  0.0000000E+00  0.1869159E+00  0.6238174E-09 -0.6555074E-02  0.3619019E-10
          4  0.0000000E+00 -0.1708125E-05  0.0000000E+00 -0.2695171E-10  0.0000000E+00 -0.2565089E-08
    
     Body #             Cx             Cy             Cz             Cl             Cm             Cn
          1 -0.1984313E-03 -0.1256364E-07  0.6004259E-01  0.1262462E-08 -0.4010076E-02 -0.2755176E-10

2.1.3 fnms.out
================

fnm.out 파일은 stability axis를 기준으로 Force & Moment coefficient를
출력하는 파일이다. 파일 구조는 다음과 같다.

.. code-block:: text

    Force and Moment Coefficients (stability axis)
    
     Body #             CL             Cy             CD             Cl             Cm             Cn
          1  0.2389490E-02  0.2739590E-02 -0.2247292E-02  0.2368874E-02  0.4717192E-03  0.1102963E-03

Body의 개수만큼 출력되며, fnm.out 파일과 마찬가지로 Unsteady 해석에서는
시간만큼 반복된다.

2.1.4 coef.dat
================

coef.dat 파일은 비정상 해석 시 시간에 따른 공력 계수를 출력한다. 파일
구조는 다음과 같다.

.. code-block:: text

     0.0000000E+00  0.0000000E+00  0.2640697E-04  0.1099121E-12 -0.2627759E-13  0.2640697E-04  0.1099121E-12
     0.5000000E-02  0.2499997E-02 -0.1621995E-03  0.4649034E+01 -0.1230019E+01  0.4065271E-04  0.4649034E+01
     0.1000000E-01  0.4999979E-02 -0.1959058E-03  0.1169421E+00 -0.3252974E-01 -0.1857008E-03  0.1169421E+00
     0.1500000E-01  0.7499930E-02 -0.1984313E-03  0.6004259E-01 -0.4010076E-02 -0.1905719E-03  0.6004261E-01
     0.2000000E-01  0.9999833E-02 -0.2081761E-03  0.8301876E-01 -0.1523560E-01 -0.1936868E-03  0.8301880E-01
     0.2500000E-01  0.1249967E-01 -0.2130626E-03  0.7043842E-01 -0.8805840E-02 -0.1976957E-03  0.7043846E-01

왼쪽부터 t, :math:`\alpha(deg), C_X, C_Z, C_M, C_D, C_L` 이다.

2.1.5 conv.dat
================

conv.dat 파일은 convergence history를 출력하는 파일이다. 파일 구조는
다음과 같다.

.. code-block:: text

      1  0.5152375E-01  0.5152375E-01
      2  0.2490170E-01 -0.2490170E-01
      ...
     11  0.2357732E-08 -0.2357732E-08
     12  0.3909122E-09 -0.3909122E-09
      1  0.5782769E-01  0.5782769E-01
      2  0.3277784E-01 -0.3277784E-01
      ...
     11  0.3103456E-08 -0.3103456E-08
     12  0.5145537E-09 -0.5145537E-09
      1  0.5779160E-02  0.5779160E-02
      2  0.1800581E-02 -0.1800581E-02
      ...
     10  0.1028236E-08 -0.1028236E-08
     11  0.1704815E-09 -0.1704815E-09

왼쪽부터 iteration 횟수,
:math:`\left|\sum_k{\Delta \left[\phi\right]}\right|, \sum_k{\Delta \left[\phi\right]}`
을 나타낸다.

2.1.6 *grid*.nXX.tXXX.plt
==========================

*grid*.nXX.tXXX.plt 파일은 Tecplot 형식의 파일로 node에서의 물성치를
나타낸다. 속도와 압력 계수의 경우 cell center에서 값을 구한 후 inverse
distance weighting을 통해 node에서의 값으로 변환한다.

n뒤에는 body의 번호, t 뒤에는 시간 순서가 매겨진다. 출력 정보는 다음과
같다.

-  X, Y, Z : Node의 좌표
-  :math:`V_X, V_Y, V_Z` : Node에서의 속도 성분
-  :math:`C_P` : Node에서의 표면 압력 계수
-  B : plot blanking을 위한 변수,

2.1.7 *grid*.nXX.tXXXc.plt
============================

*grid*.nXX.tXXc.plt 파일은 Tecplot 형식의 파일로 cell center에서의
물성치를 나타낸다.

n뒤에는 body의 번호, t 뒤에는 시간 순서가 매겨진다. 출력 정보는 다음과
같다.

-  X, Y, Z : Node의 좌표
-  :math:`V_X, V_Y, V_Z` : Cell center에서의 속도 성분
-  :math:`C_P` : Cell center에서의 표면 압력 계수
-  :math:`S_X, S_Y, S_Z` : 패널의 outward normal vector
-  :math:`\mu` : Cell center에서의 doublet strength

2.1.8 gridg.plt
================

*grid*\ g.plt 파일은 Tecplot 형식의 파일로 domain 및 wake panel을 확인할
수 있는 파일이다. 비정상 해석 시에도 한 번만 출력된다. 출력 정보는
다음과 같다.

-  X, Y, Z : Node의 좌표
-  :math:`S_X, S_Y, S_Z` : 패널의 outward normal vector


--------------------
2.2 IUPAN 사용 방법
--------------------

1. PrePanel을 통해 해석하고자 하는 grid를 Pre-processing 한다.
2. 해석 방법 및 유동 조건을 통해 Panel.inp 파일의 &calcon, &flocon을
   작성한다.
3. PrePanel 프로그램의 output인 inp.dat 파일을 참고하여 &inppan,
   &inpbdy를 작성한다.
4. PrePanel 프로그램의 output인 grid.con, grid.grd 파일과 Panel.inp
   파일을 해당 디렉토리에 위치한 후 IUPAN을 실행한다.
