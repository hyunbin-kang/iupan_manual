# IUPAN manual

IUPAN 프로그램은 인하대학교 공력 해석 및 설계 연구실에 문의할 것.

프로그램 사용법은 다음 [문서](https://hyunbin-kang.gitlab.io/iupan_manual) 를 참고하시오.